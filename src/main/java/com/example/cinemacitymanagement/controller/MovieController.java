package com.example.cinemacitymanagement.controller;

import com.example.cinemacitymanagement.entity.Movie;
import com.example.cinemacitymanagement.repository.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class MovieController {
    private static final Logger logger = LoggerFactory.getLogger( MovieController.class );

    @Autowired
    private MovieRepository movieRepository;

    @GetMapping(restURIs.MOVIES)
    public ResponseEntity<List<Movie>> findAll() {
        List<Movie> movieList = movieRepository.findAll();
//        if (!stockOptional.isPresent()) {
//            logger.severe("StockId " + stockId + " is not existed");
//            ResponseEntity.badRequest().build();
//        }
        return ResponseEntity.ok( movieList );
    }

    @GetMapping(restURIs.MOVIE)
    public ResponseEntity<Movie> findById(@PathVariable Long id) {
        Optional<Movie> movieOptional = movieRepository.findById( id );
        if (!movieOptional.isPresent()) {
            //logger.severe("MovieId " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok( movieOptional.get() );
    }

    @PostMapping(restURIs.MOVIES)
    public ResponseEntity createNew(@Valid @RequestBody Movie movie) {
//        Movie movie = null;
//        movie.setTitle(title);
        return ResponseEntity.ok( movieRepository.save( movie ) );
    }
    @PutMapping(restURIs.MOVIE)
    public ResponseEntity<Movie> updateById(@PathVariable Long id, String title) {
        Optional<Movie> movieOptional = movieRepository.findById(id);
        if (!movieOptional.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        Movie movie = movieOptional.get();
        movie.setTitle(title);

        return ResponseEntity.ok(movieRepository.save(movie));
    }

}
