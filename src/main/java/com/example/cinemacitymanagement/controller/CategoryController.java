package com.example.cinemacitymanagement.controller;

import com.example.cinemacitymanagement.entity.Category;
import com.example.cinemacitymanagement.entity.Movie;
import com.example.cinemacitymanagement.repository.CategoryRepository;
import com.example.cinemacitymanagement.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private MovieRepository movieRepository;

    @PostMapping(restURIs.CATEGORIES)
    public ResponseEntity createNew(@Valid @RequestBody Category category, @PathVariable Long movieId ) {
        Optional<Movie> movieOptional = movieRepository.findById(movieId);
        if (!movieOptional.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        Movie movie = movieOptional.get();
        movie.addCategory( category );
        categoryRepository.save( category );
        return ResponseEntity.ok( movieRepository.save( movie ));
    }
}
