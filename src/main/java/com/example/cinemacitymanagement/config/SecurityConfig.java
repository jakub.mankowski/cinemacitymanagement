package com.example.cinemacitymanagement.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http/*AuthenticationManagerBuilder auth*/) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser( "admin@admin.pl" )
//                .password( "admin" )
//                .roles( "USER" );
        http.httpBasic().disable();
        http.csrf().disable();
    }
}


