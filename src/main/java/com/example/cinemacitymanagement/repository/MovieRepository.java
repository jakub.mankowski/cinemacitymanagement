package com.example.cinemacitymanagement.repository;


import com.example.cinemacitymanagement.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie,Long> {
    public Movie findByTitle(String title);
}
