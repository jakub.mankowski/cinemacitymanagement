package com.example.cinemacitymanagement.repository;

import com.example.cinemacitymanagement.entity.User;
import org.springframework.data.repository.Repository;

public interface UserRepository extends Repository<User, Long> {

}
