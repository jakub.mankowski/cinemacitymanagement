package com.example.cinemacitymanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.cinemacitymanagement.entity.Article;

public interface ArticleRepository extends JpaRepository<Article,Long> {
//        public Movie findByTitle(String title);
}
