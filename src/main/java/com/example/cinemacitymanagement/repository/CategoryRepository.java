package com.example.cinemacitymanagement.repository;

import com.example.cinemacitymanagement.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {
}

