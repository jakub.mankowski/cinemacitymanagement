package com.example.cinemacitymanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@ToString(exclude = "password")
@Entity
@Table(name = "users")
public class User extends BaseEntity {

//    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    private String firstName;
    private String lastName;
    private String email;

    private @JsonIgnore String password;

    private String[] roles;
    @OneToMany
    private List<Ticket> tickets;

    private BigDecimal balance;

    public User(String firstName, String lastName, String email, String... roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
//        this.password = PASSWORD_ENCODER.encode(password);
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public List<Ticket> getTicketList() {
        return tickets;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.tickets = ticketList;
    }
    public void setTicketList(Ticket ticket) {
        this.tickets.add( ticket );
    }

}
