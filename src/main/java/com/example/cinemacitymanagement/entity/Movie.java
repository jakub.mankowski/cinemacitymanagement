package com.example.cinemacitymanagement.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "movies")
public class Movie extends BaseEntity {
    private String title;
    @OneToMany
    private List<Article> articles;
    @OneToMany
    private List<Category> category;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Article> getArticles() {
        return articles;
    }


    public void addArticle(Article article) {
        this.articles.add( article );
    }

    public void removeArticle(Article article) {
        this.articles.remove( article );
    }


    public void addCategory(Category category) {
        this.category.add( category );
    }
}
