package com.example.cinemacitymanagement.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "articles")
public class Article extends BaseEntity{

    private String title;
    @Column(columnDefinition="TEXT")
    private String description;
    @OneToOne
    private Link trailer;
    @OneToMany
    private List<Link> galleryLinks;
    private Date created_at;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Link> getGalleryLinks() {
        return galleryLinks;
    }

    public void setGalleryLinks(List<Link> galleryLinks) {
        this.galleryLinks = galleryLinks;
    }

    public Link getTrailer() {
        return trailer;
    }

    public void setTrailer(Link trailer) {
        this.trailer = trailer;
    }

    @JsonSerialize(using=DateSerializer.class)
    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
